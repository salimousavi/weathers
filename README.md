# Weathers

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<b>Dependencies:</b>
  <br><code> Locations </code>

<h3> How to work? </h3>
This module add many fields to <code>locations</code> vocabulay.<br>
This module get data from <code>https://api.darksky.net</code>.

<b>Fields:</b>
 <br><code> field_weather_summary </code>
 <br><code> field_weather_temperature </code>
 <br><code> field_weather_status </code>
 <br><code> field_weather_humidity </code>
 <br><code> field_weather_wind_speed </code>
 <br><code> field_weather_date </code>

<b>Route request:</b>
 <br>
 <code> api/locations </code>

<b>Response:</b>
 <br><code> name </code> Example: Tehran
 <br><code> machine_name </code> Example: tehran
 <br><code> lat </code> Example: 35.7299926
 <br><code> long </code> Example: 51.4166642
 <br><code> diff </code> Example: +3.5
 <br><code> summary </code> Example: Clear
 <br><code> temperature </code> Example: 36.41958
 <br><code> status </code> Example: clear-day
 <br><code> humidity </code> Example: 0.1
 <br><code> wind_speed </code> Example: 7.92
 <br><code> date </code> Example: 2017-07-31T12:32:12